const { users }    = require('../../models');

async function userPost(req, res) {
    const createUser = await users.create({
        username: req.body.username,
        password: req.body.password,
        level_user: req.body.level_user
    }).then(function (result) {
        return result
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:" Post Data User Success ", data: createUser}));
}

module.exports = userPost