const { users }    = require('../../models');

async function userGet(req, res) {
    const getUser = await users.findAll().then(function (result) {
        return result
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"List Cars Success ", data: getUser}));
}

module.exports = userGet