const { users }    = require('../../models');

async function userPut(req, res) {
    const updateUser = await users.create({
        username: req.body.username,
        password: req.body.password,
        level_user: req.body.level_user
    }).then(function (result) {
        return result
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"Update Data User Success ", data: updateUser}));
}

module.exports = userPut