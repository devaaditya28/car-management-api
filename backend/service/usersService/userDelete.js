const { users }      = require('../../models');
async function userDelete(req, res) {
    console.log('req params', req.params);
    const deleteUser = await users.destroy({where: {id: parseInt(req.params.id)}}).then(function (result) {
        return result;
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"Delete data user Sucsess", data:deleteUser}));
}

module.exports = userDelete