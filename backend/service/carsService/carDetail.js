const { cars }      = require('../../models');
async function carDetail(req, res) {
    console.log('params', req.params);
    const detailCar = await cars.findOne({where: {id:parseInt(req.params.id)}}).then(function(result) {
        return result
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Sucsess", display_message:"Get Detail of Car Sucsess", data:detailCar}));
}
module.exports = carDetail;