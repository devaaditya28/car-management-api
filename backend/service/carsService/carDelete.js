const { cars }      = require('../../models');
async function carDelete(req, res) {
    console.log('req params', req.params);
    const deleteCar = await cars.destroy({where: {id: parseInt(req.params.id)}}).then(function (result) {
        return result;
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:"Delete data car Sucsess", data:deleteCar}));
}

module.exports = carDelete