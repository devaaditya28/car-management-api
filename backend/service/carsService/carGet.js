const { cars }    = require('../../models');

async function carGet(req, res) {
    const getCar = await cars.findAll().then(function (result) {
        return result
    });
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({message: "Success", display_message:" List Cars Success ", data: getCar}));
}

module.exports = carGet